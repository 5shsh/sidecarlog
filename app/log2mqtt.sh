#!/bin/sh

export $(xargs < /env)

PREFIX=k3s
TOPIC=$1
MESSAGE=$2

mosquitto_pub -h "$MQTT_HOST" -t "$PREFIX/error/$TOPIC" -m "{\"message\": \"$MESSAGE\"}"